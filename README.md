[Apache ServiceComb](https://servicecomb.apache.org/) is a simple microservice developing framework, it provieds massive out-of-box features, such as discovery and registry services, configuration management, load balancing, fault tolerance and circuit breaker, rate limiting， which help build microservice quickly.  

[ServiceStage](https://www.g42cloud.com/#/product/ServiceStage) provides a serires of tools and platforms to help users manage servicecomb microservice applications better. As shown below, users can develop, construct, deploy, operate， maintain， register their applications and set loadbalance, rate limiting, downgrade, circuitbreaker, fault tolerance, black/white list, fault injection, grayscale, dashboard and so on.

![](https://gitlab.com/cloudomate/servicestage-demo-weathermap/blob/master/arch.JPG)

Weather forecast shown above is a microservice applicaiton based on ServiceComb, which can check the weather of city based on user's selection.  
1、	weathermapweb is a microservice based on node.js  
2、 fusionweather provides weather data for web UI, based on java.  
3、 forecast service provides weather forecasts for the next few days in a specified city, based on java  
4、 weather service provides current weather information in specified city, based on java  
5、	weather-beta is the newer version of weather，it will add the ultraviolet rays information to weather microserive, based on java 

To deploy it on servicestage： [ServiceComb on ServiceStage](https://docs.g42cloud.com/usermanual/servicestage/servicestage_user_0484.html)  

Runtime interface is as shown below

![](https://gitlab.com/cloudomate/servicestage-demo-weathermap/blob/master/weathermap.JPG)
